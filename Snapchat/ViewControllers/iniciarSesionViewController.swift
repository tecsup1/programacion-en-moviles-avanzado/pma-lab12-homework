
import UIKit
import Firebase
import FirebaseAuth

class iniciarSesionViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBAction func registrarTapped(_ sender: Any) {
        performSegue(withIdentifier: "registrarUsuarioSegue", sender: nil)
    }
    
    
    @IBAction func iniciarSesionTapped(_ sender: Any) {
        
        if( emailTextField.text! == "" || passwordTextField.text! == ""){
            // MENSAJE DE ALERTA POR CAMPOS DE TEXTO VACIOS
            let alerta = UIAlertController(title: "Por favor no deje campos vacíos", message: "El campo de usuario y el campo de contraseña tienen que estar llenados.", preferredStyle: .alert)
            let btnOK = UIAlertAction(title: "Aceptar", style: .default, handler: {
                (UIAlertAction) in
            })
            alerta.addAction(btnOK)
            self.present(alerta, animated: true, completion: nil)
            
        }else{
            Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!){
                (user, error) in
                print("Intentando iniciar sesión")
                if error != nil{
                    print("Se presentó el siguiente error: \(error!)")
                    
                    // MENSAJE DE ALERTA PARA CREAR NUEVO USUARIO
                    let alerta = UIAlertController(title: "El usuario indicado no existe", message: "El Usuario: \(self.emailTextField.text!) primero debe ser creado.", preferredStyle: .alert)
                    let btnOK = UIAlertAction(title: "Crear", style: .default, handler: {
                        (UIAlertAction) in
                        self.performSegue(withIdentifier: "registrarUsuarioSegue", sender: nil)
                    })
                    let btnCancel = UIAlertAction(title: "Cancelar", style: .default, handler:{
                        (UIAlertAction) in
                    })
                    alerta.addAction(btnOK)
                    alerta.addAction(btnCancel)
                    self.present(alerta, animated: true, completion: nil)
                    
                    
                    
                    // CREACIÓN DE NUEVO USUARIO EN AUTHENTICATION
                    /*Auth.auth().createUser(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!, completion:{(user, error) in
                        print("intentando crear un usuario")
                        if error != nil{
                            print("Se presentó el siguiente error al crear el usuario: \(error!)")
                        }else{
                            print("El usuario fue creado exitosamente")
                
                            // CREACIÓN DE NUEVO USUARIO EN DATABASE:
                            /*
                            Database.database().reference().child("usuarios").child(user!.user.uid).child("email").setValue(user!.user.email)
                            let alerta = UIAlertController(title: "Creación de usuario", message: "Usuario: \(self.emailTextField.text!) se creo correctamente.", preferredStyle: .alert)
                            let btnOK = UIAlertAction(title: "Aceptar", style: .default, handler: {
                                (UIAlertAction) in
                                self.performSegue(withIdentifier: "iniciarsesionsegue", sender: nil)
                            })
                            alerta.addAction(btnOK)
                            self.present(alerta, animated: true, completion: nil)
                            */
                        }
                    })*/
                    
                }else{
                    print("Inicio de sesión exitoso")
                    self.performSegue(withIdentifier: "iniciarsesionsegue", sender: nil)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "registrarUsuarioSegue"){
            let vc = segue.destination as! RegistrarUsuarioViewController
            vc.email = emailTextField.text!
            vc.password = passwordTextField.text!
        }
    }
    
    
}

