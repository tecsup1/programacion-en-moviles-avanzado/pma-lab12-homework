import UIKit
import Firebase
import FirebaseAuth


class RegistrarUsuarioViewController: UIViewController {

    // recibiendo usuario desde otro iniciarsesionviewcontroller:
    var email = String()
    var password = String()
    
    @IBOutlet weak var usuarioTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBAction func registrarTapped(_ sender: Any) {
        
        // CREACIÓN DE NUEVO USUARIO EN AUTHENTICATION
        Auth.auth().createUser(withEmail: self.usuarioTextField.text!, password: self.passwordTextField.text!, completion:{(user, error) in
           print("intentando crear un usuario")
           if error != nil{
               print("Se presentó el siguiente error al crear el usuario: \(error!)")
           }else{
               print("El usuario fue creado exitosamente")

               // CREACIÓN DE NUEVO USUARIO EN DATABASE:
               
               Database.database().reference().child("usuarios").child(user!.user.uid).child("email").setValue(user!.user.email)
               let alerta = UIAlertController(title: "Creación de usuario", message: "Usuario: \(self.usuarioTextField.text!) se creo correctamente.", preferredStyle: .alert)
               let btnOK = UIAlertAction(title: "Aceptar", style: .default, handler: {
                   (UIAlertAction) in
                   self.performSegue(withIdentifier: "usuarioRegistradoSegue", sender: nil)
               })
               alerta.addAction(btnOK)
               self.present(alerta, animated: true, completion: nil)
               
           }
        })
        
        
        
        
    }
    
    @IBAction func cancelarTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        usuarioTextField.text! = email
        passwordTextField.text! = password
        
    }
    


}
